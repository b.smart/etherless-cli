/*
 * MIT License
 *
 * Copyright (c) 2020 B.smart
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
 
import {
    EtherlessException,
    EtherlessServerInterface,
    EtherlessSmartReadOnlyInterface,
    EthereumConnectionConfigurationInterface,
    SoftwareDetails,
    DerivedKeyInterface
} from "@b-smart/etherless-types";
import {NotInitializedException} from "./exceptions/NotInitializedException";
import {EtherlessServerException} from "./exceptions/EtherlessServerException";
import {Token, InvalidTokenException} from "@b-smart/etherless-smart";
import chalk from "chalk";
 
export class ReadOnlyCommandsFacade {
 
    private readonly etherlessInstance: EtherlessSmartReadOnlyInterface
    private readonly serverInstance: EtherlessServerInterface
 
    constructor(etherlessInstance: EtherlessSmartReadOnlyInterface, serverInstance: EtherlessServerInterface) {
        this.etherlessInstance = etherlessInstance;
        this.serverInstance = serverInstance;
    }
 
    public async init(outdatedConfiguration: EthereumConnectionConfigurationInterface, address: string, derivedKey: DerivedKeyInterface): Promise<EthereumConnectionConfigurationInterface> {
        console.log("Initializing etherless...");
 
        try {
            const updatedConfiguration = await this.serverInstance.init(outdatedConfiguration, address, derivedKey);
 
            console.log(`${chalk.green("succeeded")}. You can now use all the great ${chalk.italic("power")} that comes from etherless!`);
 
            return updatedConfiguration;
        } catch (ex) {
            const typedException: EtherlessException = ex as EtherlessException;
 
            console.error(typedException.message);
 
            if (ex instanceof EtherlessException) {
                if (ex instanceof EtherlessServerException) {
                    console.error(`${chalk.red("Error")}. An error occurred while elaborating your request on the server.`);
                } else {
                    console.error(`${chalk.red("Error")}. Unknown error while downloading the configuration from the server.`);
                }
 
                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
 
        return outdatedConfiguration;
    }
 
    public async find(softwareName: string): Promise<void> {
        console.log(`Searching ${chalk.blue(softwareName)} on etherless...`);
 
        try {
            const exists: boolean = await this.etherlessInstance.softwareExists(softwareName);
 
            if (exists) {
                console.log(`The software ${chalk.blue(softwareName)} does exists on etherless!`);
                console.log("");
 
                const details = await this.etherlessInstance.getSoftwareDetails(softwareName);
                console.log(`Name: ${details.name}`);
                console.log(`Publication Date: ${details.publishDate.toLocaleString()}`);
                console.log(`Last Update Date: ${details.lastUpdateDate.toLocaleString()}`);
                console.log(`Updated: ${details.updateCount} times`);
                console.log(`Cost: ${details.invokeCost.toString()}`);
 
            } else {
                console.log(`The software ${chalk.blue(softwareName)} does ${chalk.red("not")} exists on etherless!`);
            }
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof NotInitializedException) {
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get power!${ex}`);
                } /* else if (ex instanceof SoftwareException) { // TODO: smart contract errors
                    console.error(chalk.red("Error") + ". Could not retrieve a valid token from the server.")
                } */ else {
                    console.error(`${chalk.red("Error")}. Unknown error while removing the software ${chalk.blue(name)}!`);
                }
 
                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }
 
    public async list(address: string, showOnlySelf: boolean): Promise<void> {
        console.log("Listing available software on etherless...");
 
        try {
            const softwareList: Array<Promise<SoftwareDetails>> = await this.etherlessInstance.getSoftwareList();
 
            const softwares: Array<SoftwareDetails> = await Promise.all(softwareList);
 
            const softwareNames: Array<string> = [];
 
            softwares.forEach((currentSoftware: SoftwareDetails) => {
                if ((showOnlySelf) && (currentSoftware.owner !== address)) {
                    return;
                }
                
                softwareNames.push(currentSoftware.name);
            });
 
            console.log(softwareNames.join("\n"));
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof NotInitializedException) {
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } /* else if (ex instanceof SoftwareException) { // TODO: smart contract errors
                    console.error(chalk.red("Error") + ". Could not retrieve a valid token from the server.")
                } */ else {
                    console.error(`${chalk.red("Error")}. Unknown error while removing the software ${chalk.blue(name)}!`);
                }
 
                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }
 
    public async who(address: string): Promise<void> {
        console.log("Registered identity requested...");
 
        try {
            const balance = await this.etherlessInstance.getBalance(address);
 
            console.log(`You're using the wallet: ${chalk.blue(address)}\nBalance: ${balance.toString()}`);
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof NotInitializedException) {
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } /* else if (ex instanceof SoftwareException) { // TODO: smart contract errors
                    console.error(chalk.red("Error") + ". Could not retrieve a valid token from the server.")
                } */ else {
                    console.error(`${chalk.red("Error")}. Unknown error while removing the software ${chalk.blue(name)}!`);
                }
 
                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
 
    }
 
    public async history(address: string): Promise<void> {
        console.log("Requested the list of your software executions...");
 
        try {
            (await this.etherlessInstance.getAllSoftwareExecutionByAddress(address)).forEach((value) => console.log(value.token.toString()));
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof NotInitializedException) {
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } else {
                    console.error(`${chalk.red("Error")}. Unkn  own error while removing the software ${chalk.blue(name)}!`);
                }
 
                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }
 
    public async log(address: string, derivedKey: DerivedKeyInterface, executionID: string): Promise<void> {
        console.log(`Requested the result of the execution with unique ID ${chalk.blue(executionID)} on etherless...`);
 
        try {
            const executionEvent = await this.etherlessInstance.getFirstSoftwareExecutionEvent(address, undefined, new Token(executionID), undefined);

            const executionResult = await this.serverInstance.log(executionEvent.token, address, derivedKey);
 
            console.log(executionResult.result);
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof NotInitializedException) {
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } else if (ex instanceof InvalidTokenException) {
                    console.error(`${chalk.red("Error")}. Invalid or empty execution ID`);
                } else {
                    console.error(`${chalk.red("Error")}. Unknown error while removing the software ${chalk.blue(name)}!`);
                }
 
                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }
}