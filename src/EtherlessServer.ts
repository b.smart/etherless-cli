/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as WebRequest from "web-request";

import {EtherlessServerException} from "./exceptions/EtherlessServerException";
import {EtherlessServerErrorException} from "./exceptions/EtherlessServerErrorException";
import * as crypto from "crypto";
import {Token} from "@b-smart/etherless-smart";
import { 
    EtherlessServerInterface,
    PackagedSoftware,
    SoftwareExecutionResult,
    SoftwareExecutionRequest,
    TokenInterface,
    EthereumConnectionConfigurationInterface,
    DerivedKeyInterface
} from "@b-smart/etherless-types";

export class EtherlessServer implements EtherlessServerInterface {

    public readonly hostname: string

    public readonly timeout: number

    constructor(serverHostname: string, timeout = 151000) {
        this.hostname = serverHostname;
        this.timeout = timeout;
    }
    
    public withHostname(hostname: string): EtherlessServer {
        return new EtherlessServer(hostname, this.timeout); 
    }

    public withTimeout(timeout: number): EtherlessServer {
        return new EtherlessServer(this.hostname, timeout); 
    }

    private async encrypt(plain: string, derivedKey: DerivedKeyInterface): Promise<string> {
        const iv = crypto.randomBytes(16);
        const cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(derivedKey.toString().substring(2), "hex"), iv);
        let encrypted = cipher.update(plain);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return JSON.stringify({iv: iv.toString("hex"), encryptedData: encrypted.toString("hex")});
    }

    private async decrypt(enc: string, derivedKey: DerivedKeyInterface): Promise<string> {
        const text = JSON.parse(enc);
        const iv = Buffer.from(text.iv, "hex");
        const encryptedText = Buffer.from(text.encryptedData, "hex");
        const decipher = crypto.createDecipheriv("aes-256-cbc", Buffer.from(derivedKey.toString().substring(2), "hex"), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }

    public async init(outdatedConfiguration: EthereumConnectionConfigurationInterface, address: string, derivedKey: DerivedKeyInterface): Promise<EthereumConnectionConfigurationInterface> {
        let response: WebRequest.Response<string>|undefined = undefined;

        try {
            // remove the software product to etherless-server
            response = await WebRequest.get(
                `${this.hostname}/dev/index`,
                {
                    timeout: this.timeout,
                    headers: {
                        address: address,
                        key: derivedKey.toString()
                    }
                }
            );
        } catch (ex) {
            throw new EtherlessServerException("Unable to connect to the server");
        }

        if (response.statusCode !== 200) {
            let errorMsg = "";
            try {
                errorMsg = JSON.parse(response.content).error;
            } catch (ex) {
                errorMsg = response.content;
            }

            throw new EtherlessServerErrorException(`The etherless server returned an error: ${errorMsg}`);
        }
        
        const responseContent = JSON.parse(response.content);

        if (!responseContent.result) {
            throw new EtherlessServerException(responseContent.error.toString());
        }
                
        // Retrieve Infura configuration
        const smartAddress = responseContent.smartAddress as string;
        const infuraProjectID = responseContent.infuraProjectID as string;
        const infuraNetwork = responseContent.infuraNetwork as string;
        const rpcProviderAddress = responseContent.rpcProviderAddress as string;
        const firstBlockNumber: number = responseContent.resetToBlockNumber as number;
                
        const updatedConfiguration: EthereumConnectionConfigurationInterface = outdatedConfiguration
            .withRPCProviderAddress(rpcProviderAddress)
            .withInfuraProjectID(infuraProjectID)
            .withInfuraNetwork(infuraNetwork)
            .withSmartAddress(smartAddress)
            .withFirstBlockNumber(firstBlockNumber);

        // Load etherless configuration...
        return updatedConfiguration;
    }

    public async requestToken(address: string, derivedKey: DerivedKeyInterface): Promise<Token> {
        let response: WebRequest.Response<string>|undefined = undefined;
        
        try {
            // remove the software product to etherless-server
            response = await WebRequest.get(
                `${this.hostname}/dev/token`,
                {
                    timeout: this.timeout,
                    headers: {
                        address: address,
                        key: derivedKey.toString()
                    }
                }
            );
        } catch (ex) {
            throw new EtherlessServerException("Unable to connect to the server");
        }

        if (response.statusCode !== 200) {
            let errorMsg = "";
            try {
                errorMsg = JSON.parse(response.content).error;
            } catch (ex) {
                errorMsg = response.content;
            }

            throw new EtherlessServerErrorException(`The etherless server returned an error: ${errorMsg}`);
        }
        
        const responseContent = JSON.parse(response.content);

        const token: string = `${responseContent.token}` as string;

        if (!responseContent.result) {
            throw new EtherlessServerException(responseContent.error.toString());
        }

        return new Token(await this.decrypt(token, derivedKey));
    }

    public async deploy(software: PackagedSoftware, token: TokenInterface, address: string, derivedKey: DerivedKeyInterface ): Promise<void> {
        const softwareName: string = software.name;
        const softwarePackageBase64: string = software.zipBase64;

        let response: WebRequest.Response<string>|undefined = undefined;

        try {
            // remove the software product to etherless-server
            response = await WebRequest.post(
                `${this.hostname}/dev/deploy`,
                {
                    timeout: this.timeout,
                    headers: {
                        softwareName: softwareName,
                        token: token.toString(),
                        address: address,
                        key: derivedKey.toString()
                    }
                },
                await this.encrypt(softwarePackageBase64, derivedKey)
            );
        } catch (ex) {
            throw new EtherlessServerException("Unable to connect to the server");
        }

        if (response.statusCode !== 200) {
            let errorMsg = "";
            try {
                errorMsg = JSON.parse(response.content).error;
            } catch (ex) {
                errorMsg = response.content;
            }

            throw new EtherlessServerErrorException(`The etherless server returned an error: ${errorMsg}`);
        }
        const responseContent = JSON.parse(response.content);

        if (responseContent.result === false) {
            throw new EtherlessServerException(responseContent.error.toString());
        }
    }

    public async update(software: PackagedSoftware, token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<void> {
        const softwareName: string = software.name;
        const softwarePackageBase64: string = software.zipBase64;

        let response: WebRequest.Response<string>|undefined = undefined;

        try {
            // remove the software product to etherless-server
            response = await WebRequest.post(
                `${this.hostname}/dev/edit`,
                {
                    timeout: this.timeout,
                    headers: {
                        softwareName: softwareName,
                        token: token.toString(),
                        address: address,
                        key: derivedKey.toString()
                    }
                },
                await this.encrypt(softwarePackageBase64, derivedKey)
            );
        } catch (ex) {
            throw new EtherlessServerException("Unable to connect to the server");
        }

        if (response.statusCode !== 200) {
            let errorMsg = "";
            try {
                errorMsg = JSON.parse(response.content).error;
            } catch (ex) {
                errorMsg = response.content;
            }

            throw new EtherlessServerErrorException(`The etherless server returned an error: ${errorMsg}`);
        }
        
        const responseContent = JSON.parse(response.content);

        if (responseContent.result === false) {
            throw new EtherlessServerException(responseContent.error.toString());
        }
    }

    public async remove(softwareName: string, token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<void> {
        let response: WebRequest.Response<string>|undefined = undefined;

        try {
            // remove the software product to etherless-server
            response = await WebRequest.delete(
                `${this.hostname}/dev/delete`,
                {
                    timeout: this.timeout,
                    headers: {
                        softwareName: softwareName,
                        token: token.toString(),
                        address: address,
                        key: derivedKey.toString()
                    }
                }
            );
        } catch (ex) {
            throw new EtherlessServerException("Unable to connect to the server");
        }

        if (response.statusCode !== 200) {
            throw new EtherlessServerErrorException(`Wrong status code: ${response.statusCode}`);
        }
        
        const responseContent = JSON.parse(response.content);

        if (responseContent.result === false) {
            throw new EtherlessServerException(responseContent.error.toString());
        }
    }

    public async run(request: SoftwareExecutionRequest, token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<SoftwareExecutionResult> {
        let response: WebRequest.Response<string>|undefined = undefined;

        try {
            // remove the software product to etherless-server
            response = await WebRequest.post(
                `${this.hostname}/dev/run`,
                {
                    timeout: this.timeout,
                    headers: {
                        softwareName: request.name,
                        token: token.toString(),
                        address: address,
                        key: derivedKey.toString()
                    }
                },
                await this.encrypt(
                    JSON.stringify(
                        {
                            arguments: request.parameters
                        }
                    ),
                    derivedKey
                )
            );
        } catch (ex) {
            throw new EtherlessServerException("Unable to connect to the server");
        }

        if (response.statusCode !== 200) {
            throw new EtherlessServerErrorException(`Wrong status code: ${response.statusCode}`);
        }
        
        const responseContent = JSON.parse(response.content);

        if (responseContent.result === false) {
            throw new EtherlessServerException(responseContent.error.toString());
        }

        const invokeResult = JSON.parse(await this.decrypt(responseContent.invokeResult, derivedKey));
        return new SoftwareExecutionResult(invokeResult.statusCode, invokeResult.result);
    }

    public async log(token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<SoftwareExecutionResult> {
        let response: WebRequest.Response<string>|undefined = undefined;

        try {
            // remove the software product to etherless-server
            response = await WebRequest.get(
                `${this.hostname}/dev/log`,
                {
                    timeout: this.timeout,
                    headers: {
                        token: token.toString(),
                        address: address,
                        key: derivedKey.toString()
                    }
                }
            );
        } catch (ex) {
            throw new EtherlessServerException("Unable to connect to the server");
        }

        if (response.statusCode !== 200) {
            throw new EtherlessServerErrorException(`Wrong status code: ${response.statusCode}`);
        }
            
        const responseContent = JSON.parse(response.content);

        if (responseContent.result === false) {
            throw new EtherlessServerException(responseContent.error.toString());
        }

        const invokeResult = JSON.parse(await this.decrypt(responseContent.invokeResult, derivedKey));
        return new SoftwareExecutionResult(invokeResult.statusCode, invokeResult.result);
    }
}
