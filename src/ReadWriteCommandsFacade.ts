/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {EtherlessException, SoftwareExecutionRequest, EtherlessServerInterface, EtherlessSmartReadWriteInterface, EthereumGas, EthereumAmount, InvalidEthereumAmountException} from "@b-smart/etherless-types";
import {SoftwarePackage} from "./SoftwarePackage";
import {NotInitializedException} from "./exceptions/NotInitializedException";
import {EtherlessServerException} from "./exceptions/EtherlessServerException";
import {InvalidSoftwareNameException} from "./exceptions/InvalidSoftwareNameExecption";
import {EthereumException, InvalidTokenException} from "@b-smart/etherless-smart";
import {File} from "./File";
import chalk from "chalk";

export class ReadWriteCommandsFacade {

    private readonly etherlessInstance: EtherlessSmartReadWriteInterface
    private readonly serverInstance: EtherlessServerInterface

    constructor(etherlessInstance: EtherlessSmartReadWriteInterface, serverInstance: EtherlessServerInterface) {
        this.etherlessInstance = etherlessInstance;
        this.serverInstance = serverInstance;
    }

    public async deploy(softwareName: string, softwareFilePath: string, gasLimit = 2700000, gasPrice = "9.0", gasUnit = "gwei"): Promise<void> {
        console.log(`Requested deploy of ${chalk.blue(softwareName)} on etherless, contained in the JS file ${chalk.whiteBright(softwareFilePath)} ...`);

        let softwareFileContent = "";
        try {
            softwareFileContent = await File.read(softwareFilePath);
        } catch (ex) {
            console.error(`${chalk.red("Error")}. There was an error reading your awesome software :(`);
        }

        if (softwareFileContent.length === 0) {
            return;
        }

        try {
            if (softwareName.length <= 0) {
                throw new InvalidSoftwareNameException(softwareName);
            }

            const address = await this.etherlessInstance.account.getAddress();

            const derivedKey = await this.etherlessInstance.account.getPrivateKeyHash();

            const softwarePackage = new SoftwarePackage(softwareFileContent, softwareName);

            // Get a new unique token for the current operation
            const token = await this.serverInstance.requestToken(address, derivedKey);

            // write the software to the ethereum smart contract
            await this.etherlessInstance.deploySoftware(softwareName, token, new EthereumGas(gasLimit, gasPrice, gasUnit));

            // upload the software package to the server instance
            await this.serverInstance.deploy(await softwarePackage.generatePackage(), token, address, derivedKey);

            console.log(`${chalk.green("Success")}! From now on everybody can use ${chalk.blue(softwareName)}, you gave ${chalk.italic("power")} to the world!`);
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof InvalidTokenException) {
                    console.error(`${chalk.red("Error")}. Could not retrieve a valid token from the server.`);  
                } else if (ex instanceof EtherlessServerException) {
                    console.error(`${chalk.red("Error")}. An error occurred while elaborating your request on the server.`);
                } else if (ex instanceof NotInitializedException) {
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } else if (ex instanceof InvalidSoftwareNameException) {
                    console.error(`${chalk.red("Error")}. The provided software name is not valid!`);
                } else if (ex instanceof EthereumException) {
                    console.error(`${chalk.red("Error")}. An error occurred while writing your request to the ethereum network. Probably a software with the given name already exists.`);
                } else {
                    console.error(`${chalk.red("Error")}. Unknown error while publishing the software ${chalk.blue(softwareName)}!`);
                }

                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }

    public async update(softwareName: string, softwareFilePath: string, gasLimit = 2700000, gasPrice = "9.0", gasUnit = "gwei"): Promise<void> {
        console.log(`Requested the upload of the new version of ${chalk.blue(softwareName)} on etherless, with the content of ${chalk.whiteBright(softwareFilePath)}...`);
        
        let softwareFileContent = "";
        try {
            softwareFileContent = await File.read(softwareFilePath);
        } catch (ex) {
            console.error(`${chalk.red("Error")}. There was an error reading your awesome software :(`);
        }

        if (softwareFileContent.length === 0) {
            return;
        }

        const address = await this.etherlessInstance.account.getAddress();

        const derivedKey = await this.etherlessInstance.account.getPrivateKeyHash();

        try {
            if (softwareName.length <= 0) {
                throw new InvalidSoftwareNameException(softwareName);
            }

            const softwarePackage = new SoftwarePackage(softwareFileContent, softwareName);

            // Get a new unique token for the current operation
            const token = await this.serverInstance.requestToken(address, derivedKey);

            // write the software update to the ethereum smart contract
            await this.etherlessInstance.updateSoftware(softwareName, token, new EthereumGas(gasLimit, gasPrice, gasUnit));

            // upload the software update package to the server instance
            await this.serverInstance.update(await softwarePackage.generatePackage(), token, address, derivedKey);

            console.log(`${chalk.green("Success")}! From now on everybody can use the new version of ${chalk.blue(softwareName)}, you increased ${chalk.italic("power")} on the world!`);
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof InvalidTokenException) {
                    console.error(`${chalk.red("Error")}. Could not retrieve a valid token from the server.`);  
                } else if (ex instanceof EtherlessServerException){
                    console.error(`${chalk.red("Error")}. An error occurred while elaborating your request on the server.`);
                } else if (ex instanceof NotInitializedException){
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } else if (ex instanceof InvalidSoftwareNameException) {
                    console.error(`${chalk.red("Error")}. The provided software name is not valid!`);
                } else if (ex instanceof EthereumException) {
                    console.error(`${chalk.red("Error")}. An error occurred while writing your request to the ethereum network. Probably a software with the given name doesn't exist or you are not allowed to edit it.`);
                } else {
                    console.error(`${chalk.red("Error")}. Unknown error while updating the software ${chalk.blue(softwareName)}!`);
                }

                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }

    public async remove(softwareName: string, gasLimit = 2700000, gasPrice = "9.0", gasUnit = "gwei"): Promise<void> {
        console.log(`Removing your software ${chalk.blue(softwareName)} from etherless...`);

        const address = await this.etherlessInstance.account.getAddress();

        const derivedKey = await this.etherlessInstance.account.getPrivateKeyHash();

        try {
            if (softwareName.length <= 0) {
                throw new InvalidSoftwareNameException(softwareName);
            }

            // Get a new unique token for the current operation
            const token = await this.serverInstance.requestToken(address, derivedKey);

            // remove the software from the ethereum smart contract
            await this.etherlessInstance.removeSoftware(softwareName, token, new EthereumGas(gasLimit, gasPrice, gasUnit));
            
            // remove the software package from the server
            await this.serverInstance.remove(softwareName, token, address, derivedKey);

            console.log(`${chalk.green("Success")}! You removed the software ${chalk.blue(softwareName)} from etherless, you retired your ${chalk.italic("power")} from the world!`);
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof InvalidTokenException) {
                    console.error(`${chalk.red("Error")}. Could not retrieve a valid token from the server.`);  
                } else if (ex instanceof EtherlessServerException){
                    console.error(`${chalk.red("Error")}. An error occurred while elaborating your request on the server.`);
                } else if (ex instanceof NotInitializedException){
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } else if (ex instanceof InvalidSoftwareNameException) {
                    console.error(`${chalk.red("Error")}. The provided software name is not valid!`);
                } else if (ex instanceof EthereumException) {
                    console.error(`${chalk.red("Error")}. An error occurred while writing your request to the ethereum network. Probably a software with the given name doesn't exist or you are not allowed to remove it.`);
                } else {
                    console.error(`${chalk.red("Error")}. Unknown error while removing the software ${chalk.blue(softwareName)}!`);
                }

                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }

    public async run(softwareName: string, args: Array<string>, gasLimit = 3000000, gasPrice = "9.0", gasUnit = "gwei"): Promise<void> {
        let text = `Requested the execution of ${chalk.blue(softwareName)} on etherless...`;
        args.forEach((item, index) => {
            text += `\n    arg[${index}] ${item}`;
        });
        console.log(text);

        const address = await this.etherlessInstance.account.getAddress();

        const derivedKey = await this.etherlessInstance.account.getPrivateKeyHash();

        try {
            if (softwareName.length <= 0) {
                throw new InvalidSoftwareNameException(softwareName);
            }

            const request = new SoftwareExecutionRequest(softwareName, args);

            // Get a new unique token for the current operation
            const token = await this.serverInstance.requestToken(address, derivedKey);

            // pay for the software execution
            await this.etherlessInstance.invokeSoftware(request.name, token, new EthereumGas(gasLimit, gasPrice, gasUnit));

            const result = await this.serverInstance.run(request, token, address, derivedKey);

            console.log(`${chalk.green("Success")}! ${chalk.blue(softwareName)}, has been executed!`);

            console.log(result.result);
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof InvalidTokenException) {
                    console.error(`${chalk.red("Error")}. Could not retrieve a valid token from the server.`);  
                } else if (ex instanceof EtherlessServerException) {
                    console.error(`${chalk.red("Error")}. An error occurred while elaborating your request on the server.`);
                } else if (ex instanceof NotInitializedException) {
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } else if (ex instanceof InvalidSoftwareNameException) {
                    console.error(`${chalk.red("Error")}. The provided software name is not valid!`);
                } else if (ex instanceof EthereumException) {
                    console.error(`${chalk.red("Error")}. An error occurred while writing your request to the ethereum network. Probably a software with the given name doesn't exist or you have insufficiend funds deposited.`);
                } else {
                    console.error(`${chalk.red("Error")}. Unknown error while running the software ${chalk.blue(softwareName)}!`);
                }

                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }

    public async deposit(amount: string, unitIn: string, gasLimit = 2700000, gasPrice = "9.0", gasUnit = "gwei"): Promise<void> {
        console.log("Charging ETH on etherless...");

        const unit = ((unitIn === "eth") || (unitIn === "ethers")) ? "ether" : unitIn;

        try {
            // remove the software from the ethereum smart contract
            await this.etherlessInstance.deposit(new EthereumAmount(amount, unit), new EthereumGas(gasLimit, gasPrice, gasUnit));
            
            console.log(`${chalk.green("Success")}! You deposited ${chalk.grey(`${amount} ${unit}`)} on etherless for software executions!`);
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof InvalidEthereumAmountException) {
                    console.error(`${chalk.red("Error")}. Invalid amount of ethers specified, use [0-9]+(.[0-9])? gwei|wei|eth.`);
                } else if (ex instanceof NotInitializedException){
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } else if (ex instanceof EthereumException) {
                    console.error(`${chalk.red("Error")}. An error occurred while writing your request to the ethereum network. The specified amount is probably wrong.`);
                } else {
                    console.error(`${chalk.red("Error")}. Unknown error while depositing ethers on etherless!`);
                }

                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }

    public async withdraw(gasLimit = 2700000, gasPrice = "9.0", gasUnit = "gwei"): Promise<void> {
        console.log("Retrieving ETH from etherless...");

        try {
            // remove the software from the ethereum smart contract
            await this.etherlessInstance.withdraw(new EthereumGas(gasLimit, gasPrice, gasUnit));
            
            console.log(`${chalk.green("Success")}! You retrieved your ETH(s) from etherless!`);
        } catch (ex) {
            if (ex instanceof EtherlessException) {
                if (ex instanceof NotInitializedException){
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                } else if (ex instanceof EthereumException) {
                    console.error(`${chalk.red("Error")}. An error occurred while writing your request to the ethereum network.`);
                } else {
                    console.error(`${chalk.red("Error")}. Unknown error while withdrawing ethers from etherless!`);
                }

                console.error(ex.message);
            } else {
                console.error(ex);
            }
        }
    }
}