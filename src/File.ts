/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import fs from "fs";

export class File {

    public static async delete(file: string): Promise<void> {
        return fs.unlinkSync(file);
    }

    public static async read(file: string): Promise<string> {
        return fs.readFileSync(file, {
            encoding: "utf8",
            flag: "r"
        });
    }

    public static async readJSON(file: string): Promise<any> {
        return JSON.parse(
            fs.readFileSync(
                file, {
                    encoding: "utf8",
                    flag: "r"
                }
            )
        );
    }

    public static async write(file: string, content: string|Buffer|Uint8Array): Promise<void> {
        return fs.writeFileSync(
            file,
            content,
            {
                encoding: "utf8",
                flag: "w+"
            }
        );
    }

    public static async writeJSON(file: string, content: any): Promise<void> {
        return fs.writeFileSync(
            file,
            JSON.stringify(content, null, "\t"),
            {
                encoding: "utf8",
                flag: "w+"
            }
        );
    }
}