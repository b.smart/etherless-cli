/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {File} from "./File";
import {EthereumConnectionConfiguration} from "@b-smart/etherless-smart";
import {NotInitializedException} from "./exceptions/NotInitializedException";

export class EtherlessCLIConfiguration extends EthereumConnectionConfiguration {

    public readonly serverHostname: string

    public readonly privateKey: string

    constructor(
        serverHostname = "",
        infuraProjectID = "",
        infuraNetwork = "",
        rpcProviderAddress = "",
        smartAddress = "",
        resetToBlockNumber = 0,
        privateKey = "",
    ) {
        super(infuraProjectID, infuraNetwork, rpcProviderAddress, smartAddress, resetToBlockNumber);
        this.serverHostname = serverHostname;
        this.privateKey = privateKey;
    }

    public static async loadFromFile(filePath: string): Promise<EtherlessCLIConfiguration>  {
        try {
            const configuration = await File.readJSON(filePath);

            return new EtherlessCLIConfiguration(
                configuration.serverHostname,
                configuration.infuraProjectID,
                configuration.infuraNetwork,
                configuration.rpcProviderAddress,
                configuration.smartAddress,
                configuration.resetToBlockNumber,
                configuration.privateKey,
            );
        } catch (ex) {
            throw new NotInitializedException();
        }
    }
    
    public static async storeToFile(configuration: EtherlessCLIConfiguration, filePath: string): Promise<void> {
        await File.writeJSON(filePath, configuration);
    }

    public withRPCProviderAddress(rpcProviderAddress: string): EtherlessCLIConfiguration {
        return new EtherlessCLIConfiguration(
            this.serverHostname,
            this.infuraProjectID,
            this.infuraNetwork,
            rpcProviderAddress,
            this.smartAddress,
            this.resetToBlockNumber,
            this.privateKey,
        );
    }

    public withInfuraProjectID(infuraProjectID: string): EtherlessCLIConfiguration {
        return new EtherlessCLIConfiguration(
            this.serverHostname,
            infuraProjectID,
            this.infuraNetwork,
            this.rpcProviderAddress,
            this.smartAddress,
            this.resetToBlockNumber,
            this.privateKey,
        );
    }

    public withInfuraNetwork(infuraNetwork: string): EtherlessCLIConfiguration {
        return new EtherlessCLIConfiguration(
            this.serverHostname,
            this.infuraProjectID,
            infuraNetwork,
            this.rpcProviderAddress,
            this.smartAddress,
            this.resetToBlockNumber,
            this.privateKey,
        );
    }

    public withSmartAddress(smartAddress: string): EtherlessCLIConfiguration {
        return new EtherlessCLIConfiguration(
            this.serverHostname,
            this.infuraProjectID,
            this.infuraNetwork,
            this.rpcProviderAddress,
            smartAddress,
            this.resetToBlockNumber,
            this.privateKey,
        );
    }

    public withPrivateKey(privateKey: string): EtherlessCLIConfiguration {
        return new EtherlessCLIConfiguration(
            this.serverHostname,
            this.infuraProjectID,
            this.infuraNetwork,
            this.rpcProviderAddress,
            this.smartAddress,
            this.resetToBlockNumber,
            privateKey,
        );
    }

    public withServerHostname(serverHostname: string): EtherlessCLIConfiguration {
        return new EtherlessCLIConfiguration(
            serverHostname,
            this.infuraProjectID,
            this.infuraNetwork,
            this.rpcProviderAddress,
            this.smartAddress,
            this.resetToBlockNumber,
            this.privateKey,
        );
    }

    public withFirstBlockNumber(resetToBlockNumber: number): EtherlessCLIConfiguration {
        return new EtherlessCLIConfiguration(
            this.serverHostname,
            this.infuraProjectID,
            this.infuraNetwork,
            this.rpcProviderAddress,
            this.smartAddress,
            resetToBlockNumber,
            this.privateKey,
        );
    }

}