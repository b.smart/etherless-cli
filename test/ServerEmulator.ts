/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
    EtherlessException,
    //File,

    //inizio di superlista 
    EtherlessSmartReadWriteInterface,
    EtherlessSoftwareUpgradeOperation,
    EtherlessServerInterface,
    EthereumGas,
    EthereumAmount,
    EtherlessSoftwareOperationInterface,
    EtherlessSoftwareDeployOperation,
    EtherlessSoftwareRemovalOperation,
    EtherlessSoftwareExecutionOperation,
    TokenInterface,
    EthereumAccountInterface, EthereumConnectionConfigurationInterface, PackagedSoftware, SoftwareExecutionRequest, SoftwareExecutionResult
    //fine superlista

    //SoftwareExecutionRequest,
    //EtherlessServerInterface
} from '@b-smart/etherless-types';
import { EthereumAccount, EthereumConnectionConfiguration, Token } from '@b-smart/etherless-smart';
import { ReadOnlyCommandsFacade } from '../src/ReadOnlyCommandsFacade';
import { ReadWriteCommandsFacade } from '../src/ReadWriteCommandsFacade';

import { EtherlessServerException } from "../src/exceptions/EtherlessServerException";

import { EthereumEmulator } from './EthereumEmulator'

import JSZip from 'jszip';

export class ServerEmulator implements EtherlessServerInterface {

    private emulatedBlockchain: EthereumEmulator

    private publishedSoftware: Map<string, string> = new Map()

    private execResults: Map<string, SoftwareExecutionResult> = new Map()

    public readonly account: EthereumAccountInterface;

    constructor(emulatedBlockchain: EthereumEmulator, account: EthereumAccountInterface) {
        this.emulatedBlockchain = emulatedBlockchain
        this.account = account
    }

    public async init(outdatedConfiguration: EthereumConnectionConfigurationInterface): Promise<EthereumConnectionConfigurationInterface> {
        return outdatedConfiguration;
    }

    async requestToken(): Promise<TokenInterface> {
        return Token.generateToken()
    }

    async deploy(software: PackagedSoftware, token: TokenInterface): Promise<void> {
        const zip = JSZip()
        const unzipped = await zip.loadAsync(software.getZipBuffer());

        const zipData = await unzipped.files['index.js'].async('text');

        this.publishedSoftware.set(software.name, zipData);
    }

    async run(request: SoftwareExecutionRequest, token: TokenInterface): Promise<SoftwareExecutionResult> {
        const jsData = this.publishedSoftware.get(request.name);

        if (jsData === undefined) {
            throw new EtherlessServerException("Software not found");
        }

        const result = eval(jsData + ";\n handler(" + JSON.stringify(request.parameters) + ");");

        const execResult = new SoftwareExecutionResult(result.statusCode, result.body)

        this.execResults.set(token.toString(), execResult)

        return execResult
    }

    async update(software: PackagedSoftware, token: Token): Promise<void> {
        const zip = JSZip()
        const unzipped = await zip.loadAsync(software.getZipBuffer());

        const zipData = await unzipped.files['index.js'].async('text');

        this.publishedSoftware.set(software.name, zipData);
    }

    async remove(softwareName: string, token: Token): Promise<void> {
        this.publishedSoftware.delete(softwareName);
        //this.execResults.delete(token.toString())
    }

    async log(token: TokenInterface): Promise<SoftwareExecutionResult> {
        const result = this.execResults.get(token.toString())
        //console.log(result.string)
        if (typeof result === "undefined")
            throw new EtherlessServerException("Log not found");

        return result;
    }

}