 function removeDirtyCharacters(dirtyString: string): string {

    dirtyString = dirtyString.replace("\u001b[39m", "");
    dirtyString = dirtyString.replace("\u001b[3m", "");
    dirtyString = dirtyString.replace("\u001b[23m", "");
    dirtyString = dirtyString.replace("\u001b[31m", "");
    dirtyString = dirtyString.replace("\u001b[32m", "");
    dirtyString = dirtyString.replace("\u001b[34m", "");
    dirtyString = dirtyString.replace("\u001b[39m", "");
    dirtyString = dirtyString.replace("\u001b[90m", "");
    dirtyString = dirtyString.replace("\u001b[97m", "");
    dirtyString = dirtyString.trim();

    return dirtyString;

}

export function cleanStringArray(dirtyArray: Array<string>): Array<string> {

    /*Clean strings from characters derived from chalk */
    let cleaned: Array<string> = [];
    dirtyArray.forEach((dirtyString: string) => {
        cleaned.push(removeDirtyCharacters(dirtyString));
    });
    /*Clean strings from characters derived from chalk */

    return cleaned;

}