

import { ReadOnlyCommandsFacade } from '../src/ReadOnlyCommandsFacade';
import { ReadWriteCommandsFacade } from '../src/ReadWriteCommandsFacade';

import { DerivedKeyInterface, EthereumAccountInterface } from '@b-smart/etherless-types';
import { EthereumAccount } from '@b-smart/etherless-smart';

import { EthereumEmulator } from '../test/EthereumEmulator';
import { ServerEmulator } from '../test/ServerEmulator';

let stdout = require("test-console").stdout;
var stderr = require("test-console").stderr;

import { cleanStringArray } from "./utils"

export async function initFunction(facade: ReadOnlyCommandsFacade, ethereumInstance: EthereumEmulator, address: string, derivedKey: DerivedKeyInterface): Promise<Array<string>> {

    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await facade.init(ethereumInstance.connectionConfiguration, address, derivedKey);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function deployFunction(swName: string, swPath: string, writeFacade: ReadWriteCommandsFacade, ethereumInstance: EthereumEmulator): Promise<Array<string>> {

    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await writeFacade.deploy(swName, swPath);

    inspect.restore();
    inspectError.restore();
    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}


export async function deleteFunction(swName: string, writeFacade: ReadWriteCommandsFacade): Promise<Array<string>> {

    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await writeFacade.remove(swName);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function findFunction(swName: string, facade: ReadOnlyCommandsFacade): Promise<Array<string>> {

    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await facade.find(swName);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function listFunction(facade: ReadOnlyCommandsFacade, account: EthereumAccountInterface, showOnlySelf: boolean): Promise<Array<string>> {
    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    const address = await account.getAddress();

    await facade.list(address, showOnlySelf);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function whoFunction(facade: ReadOnlyCommandsFacade, account: EthereumAccount): Promise<Array<string>> {
    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await facade.who(account.privateKey);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function historyFunction(account: EthereumAccountInterface, facade: ReadOnlyCommandsFacade): Promise<Array<string>> {
    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    const address = await account.getAddress();

    await facade.history(address);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function runFunction(swName: string, args: Array<string>, writeFacade: ReadWriteCommandsFacade): Promise<Array<string>> {
    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await writeFacade.run(swName, args);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function logFunction(account: EthereumAccountInterface, token: string, facade: ReadOnlyCommandsFacade, derivedKey: DerivedKeyInterface): Promise<Array<string>> {
    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    const address = await account.getAddress();

    await facade.log(address, derivedKey, token);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function updateFunction(swName: string, swPath: string, writeFacade: ReadWriteCommandsFacade): Promise<Array<string>> {
    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await writeFacade.update(swName, swPath);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function depositFunction(amount: string, unit: string, writeFacade: ReadWriteCommandsFacade): Promise<Array<string>> {
    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await writeFacade.deposit(amount, unit);

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}

export async function withdrawFunction(writeFacade: ReadWriteCommandsFacade): Promise<Array<string>> {
    let inspect = stdout.inspect();
    let inspectError = stderr.inspect();

    await writeFacade.withdraw();

    inspect.restore();
    inspectError.restore();

    const cleanOutput: Array<string> = cleanStringArray(inspect.output.concat(inspectError.output));

    return cleanOutput;
}