/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import 'mocha';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);
const expect = chai.expect;

var assert = require('assert');

let stdout = require("test-console").stdout;
var stderr = require("test-console").stderr;

import {
    EtherlessException,
    //File,

    //inizio di superlista 
    EtherlessSmartReadWriteInterface,
    EtherlessSoftwareUpgradeOperation,
    EtherlessServerInterface,
    EthereumGas,
    EthereumAmount,
    EtherlessSoftwareOperationInterface,
    EtherlessSoftwareDeployOperation,
    EtherlessSoftwareRemovalOperation,
    EtherlessSoftwareExecutionOperation,
    TokenInterface,
    EthereumAccountInterface, EthereumConnectionConfigurationInterface, PackagedSoftware, SoftwareExecutionRequest, SoftwareExecutionResult
    //fine superlista

    //SoftwareExecutionRequest,
    //EtherlessServerInterface
} from '@b-smart/etherless-types';
import { EthereumAccount, EthereumConnectionConfiguration, Token, EthereumException, InvalidTokenException } from '@b-smart/etherless-smart';
import { EtherlessServer } from '../src/EtherlessServer';
import { ReadOnlyCommandsFacade } from '../src/ReadOnlyCommandsFacade';
import { ReadWriteCommandsFacade } from '../src/ReadWriteCommandsFacade';
import { EthereumEmulator } from '../test/EthereumEmulator';
import { ServerEmulator } from '../test/ServerEmulator';


import {
    initFunction,
    deployFunction,
    updateFunction,
    runFunction,
    deleteFunction,
    findFunction,
    listFunction,
    withdrawFunction,
    depositFunction,
    logFunction,
    historyFunction,
    whoFunction
} from "./commands";


describe('Deploy and remove a software correctly', () => {
    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('find: correctly find the software deployed', async () => {

        const date: string = (new Date()).toLocaleString();
        const logs: Array<string> = await findFunction(swName, facade);
        //execute and retrieve the console logs and errors of find command

        assert.deepEqual(logs, [
            `Searching ${swName} on etherless...`,
            'The software testSoftware does exists on etherless!',
            '',
            `Name: ${swName}`,
            `Publication Date: ${date}`,
            `Last Update Date: ${date}`,
            'Updated: 0 times',
            'Cost: 1000 gwei'
        ]);
    });

    it('delete: correctly delete a software', async () => {

        const logs: Array<string> = await deleteFunction(swName, writeFacade);
        //execute and retrieve the console logs and errors of delete command

        assert.deepEqual(logs, [
            `Removing your software ${swName} from etherless...`,
            `Success! You removed the software ${swName} from etherless, you retired your power from the world!`,
        ]);
    });

    it('find: cannot find the removed software', async () => {

        const logs: Array<string> = await findFunction(swName, facade);
        //execute and retrieve the console logs and errors of find command

        assert.deepEqual(logs, [
            `Searching ${swName} on etherless...`,
            "The software testSoftware does not exists on etherless!"
        ]);
    });

});

describe('Deploy correctly but fail to remove the software', () => {
    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('find: correctly find the software deployed', async () => {

        const logs: Array<string> = await findFunction(swName, facade);
        //execute and retrieve the console logs and errors of find command

        let timeObject: Date = new Date();
        const date: string = timeObject.toLocaleString();

        assert.deepEqual(logs, [
            `Searching ${swName} on etherless...`,
            'The software testSoftware does exists on etherless!',
            '',
            `Name: ${swName}`,
            `Publication Date: ${date}`,
            `Last Update Date: ${date}`,
            'Updated: 0 times',
            'Cost: 1000 gwei'
        ]);
    });

    it('delete: fail to delete a software', async () => {
        const wrongSwName = swName + "1";
        const logs: Array<string> = await deleteFunction(wrongSwName, writeFacade);
        //execute and retrieve the console logs and errors of delete command

        assert.deepEqual(logs, [
            `Removing your software ${wrongSwName} from etherless...`,
            `Error. Unknown error while removing the software ${wrongSwName}!`,
            `Software must exists to be removed`
        ]);
    });
});

describe('Execute a software and log the result', () => {
    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";
    let historyLogs: Array<string> = [];

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });
    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('run: correctly run the deployed software', async () => {

        const args: Array<string> = ["1", "2", "3"];

        const logs: Array<string> = await runFunction(swName, args, writeFacade);

        // console.log(logs);
        //execute and retrieve the console logs and errors of run command

        assert.deepEqual(logs, [
            'Requested the execution of testSoftware on etherless...\n' +
            '    arg[0] 1\n' +
            '    arg[1] 2\n' +
            '    arg[2] 3',
            'Success! testSoftware, has been executed!',
            '{\n' +
            '  "result": "sum v1.0.0, result is: 6",\n' +
            '  "input": [\n' +
            '    "1",\n' +
            '    "2",\n' +
            '    "3"\n' +
            '  ]\n' +
            '}'
        ]);
    });

    it('history: correctly get the token of the executions', async () => {

        historyLogs = await historyFunction(ethereumInstance.account, facade);
        //execute and retrieve the console logs and errors of history command

        assert.deepEqual(historyLogs, [
            "Requested the list of your software executions...",
            historyLogs[1]
        ]);
    })

    it('log: correctly log the function execution result', async () => {

        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await logFunction(ethereumInstance.account, historyLogs[1], facade, privateKey);
        //execute and retrieve the console logs and errors of log command

        var token = logs[0].substr(
            logs[0].lastIndexOf("ID") + 3,
            66
        );

        assert.deepEqual(logs, [
            `Requested the result of the execution with unique ID ${token} on etherless...`,
            "{\n  \"result\": \"sum v1.0.0, result is: 6\",\n  \"input\": [\n    \"1\",\n    \"2\",\n    \"3\"\n  ]\n}"
        ]);
    });

});

describe('Correctly update the function', () => {

    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('update: correctly update the previous deployed software', async () => {

        const logs: Array<string> = await updateFunction(swName, "./test/examples/sum.js", writeFacade);
        //execute and retrieve the console logs and errors of edit command

        assert.deepEqual(logs, [
            `Requested the upload of the new version of ${swName} on etherless, with the content of ./test/examples/sum.js...`,
            `Success! From now on everybody can use the new version of ${swName}, you increased power on the world!`,
        ]);

    });
});

describe('Cannot update the function because the path is wrong', () => {

    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('update: cannot update the software with the path given', async () => {

        const logs: Array<string> = await updateFunction(swName, "../exampl/sum.js", writeFacade);
        //execute and retrieve the console logs and errors of edit command

        assert.deepEqual(logs, [
            `Requested the upload of the new version of ${swName} on etherless, with the content of ../exampl/sum.js...`,
            `Error. There was an error reading your awesome software :(`,
        ]);

    });
});

describe('Cannot update the function because the software name given doesn\'t exist', () => {

    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('update: cannot update the software with the given name', async () => {
        const wrongSwName = swName + "1";
        const logs: Array<string> = await updateFunction(wrongSwName, "./test/examples/sum.js", writeFacade);
        //execute and retrieve the console logs and errors of edit command

        assert.deepEqual(logs, [
            `Requested the upload of the new version of ${wrongSwName} on etherless, with the content of ./test/examples/sum.js...`,
            `Error. Unknown error while updating the software ${wrongSwName}!`,
            `The software specified does not exists`
        ]);

    });
});

describe('Deposit and withdraw correctly', () => {
    const ethereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");
    const ethereumInstance = new EthereumEmulator(ethereumAccount);
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);
    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const amount: string = "1000";
    const unit: string = "gwei";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('who: should return the account that is being used', async () => {

        const logs = await whoFunction(facade, ethereumAccount);
        //execute and retrieve the console logs and errors of who command

        assert.deepEqual(logs, [
            "Registered identity requested...",
            "You're using the wallet: 0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df\nBalance: 50 gwei"
        ]);

    });

    it('deposit: correctly deposit the specified amount', async () => {

        const logs: Array<string> = await depositFunction("1000", "gwei", writeFacade);
        //execute and retrieve the console logs and errors of deposit command

        assert.deepEqual(logs, [
            "Charging ETH on etherless...",
            `Success! You deposited ${amount} ${unit} on etherless for software executions!`
        ]);
    });

    it('withdraw: correctly withdraw all the deposit amount', async () => {

        const logs: Array<string> = await withdrawFunction(writeFacade);
        //execute and retrieve the console logs and errors of withdraw command

        assert.deepEqual(logs, [
            "Retrieving ETH from etherless...",
            "Success! You retrieved your ETH(s) from etherless!"
        ]);
    });

});

describe('Execute a software and cannot log the result (invalid token representation)', async () => {
    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('run: correctly run the deployed software', async () => {

        const args: Array<string> = ["1", "2", "3"];

        const logs: Array<string> = await runFunction(swName, args, writeFacade);
        //execute and retrieve the console logs and errors of run command

        assert.deepEqual(logs, [
            'Requested the execution of testSoftware on etherless...\n' +
            '    arg[0] 1\n' +
            '    arg[1] 2\n' +
            '    arg[2] 3',
            'Success! testSoftware, has been executed!',
            '{\n' +
            '  "result": "sum v1.0.0, result is: 6",\n' +
            '  "input": [\n' +
            '    "1",\n' +
            '    "2",\n' +
            '    "3"\n' +
            '  ]\n' +
            '}'
        ]);
    });

    it('history: correctly get the token of the executions', async () => {

        const historyLogs = await historyFunction(ethereumInstance.account, facade);
        //execute and retrieve the console logs and errors of history command

        assert.deepEqual(historyLogs, [
            "Requested the list of your software executions...",
            historyLogs[1]
        ]);
    });

    it('log: should give the correct log for the error invalid token', async () => {

        const logs = await logFunction(ethereumInstance.account, "", facade, ethereumInstance.account.getPrivateKeyHash());
        //execute and retrieve the console logs and errors of log command

        assert.deepEqual(logs, [
            "Requested the result of the execution with unique ID  on etherless...",
            "Error. Invalid or empty execution ID",
            "Invalid token representation:"
        ]);
    });
});

describe('Execute a software and cannot log the result (wrong token)', async () => {
    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('run: correctly run the deployed software', async () => {

        const args: Array<string> = ["1", "2", "3"];

        const logs: Array<string> = await runFunction(swName, args, writeFacade);
        //execute and retrieve the console logs and errors of run command

        assert.deepEqual(logs, [
            'Requested the execution of testSoftware on etherless...\n' +
            '    arg[0] 1\n' +
            '    arg[1] 2\n' +
            '    arg[2] 3',
            'Success! testSoftware, has been executed!',
            '{\n' +
            '  "result": "sum v1.0.0, result is: 6",\n' +
            '  "input": [\n' +
            '    "1",\n' +
            '    "2",\n' +
            '    "3"\n' +
            '  ]\n' +
            '}'
        ]);
    });

    it('history: correctly get the token of the executions', async () => {

        const historyLogs = await historyFunction(ethereumInstance.account, facade);
        //execute and retrieve the console logs and errors of history command

        assert.deepEqual(historyLogs, [
            "Requested the list of your software executions...",
            historyLogs[1]
        ]);
    });

    it('log: should not find the operation execution because the token does NOT match any execution', async () => {

        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await logFunction(ethereumInstance.account, 'UTNOMKF0RMT623G1KQFEK2IKX68FJIN5KI9UKKFY31UJW1D8XSOGORGP5MG2A6WQ9P', facade, privateKey);
        //execute and retrieve the console logs and errors of log command

        assert.deepEqual(logs, [
            "Requested the result of the execution with unique ID UTNOMKF0RMT623G1KQFEK2IKX68FJIN5KI9UKKFY31UJW1D8XSOGORGP5MG2A6WQ9P on etherless...",
            "ExecutionOperation event not found"
        ]);
    });
});

describe('List all published software', async () => {
    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('list: should show the software list, which is empty', async () => {

        const logs: Array<string> = await listFunction(facade, new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"), false);
        //execute and retrieve the console logs and errors of list command

        assert.deepEqual(logs, [
            "Listing available software on etherless...",
            logs[1]
        ]);
    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('list: should show the software list, showing only one deployed software', async () => {

        const logs: Array<string> = await listFunction(facade, new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"), false);
        //execute and retrieve the console logs and errors of list command

        assert.deepEqual(logs, [
            "Listing available software on etherless...",
            logs[1]
        ]);
    });
});

describe('Deposit an invalid value', () => {    
    const ethereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");
    const ethereumInstance = new EthereumEmulator(ethereumAccount);
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);
    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('who: should return the account that is being used', async () => {

        const logs = await whoFunction(facade, ethereumAccount);
        //execute and retrieve the console logs and errors of who command

        assert.deepEqual(logs, [
            "Registered identity requested...",
            "You're using the wallet: 0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df\nBalance: 50 gwei"
        ]);

    });

    it('deposit: the amount and the unit are invalid', async () => {

        const logs: Array<string> = await depositFunction("mille", "euro", writeFacade);
        //execute and retrieve the console logs and errors of deposit command

        assert.deepEqual(logs, [
            "Charging ETH on etherless...",
            "Error. Invalid amount of ethers specified, use [0-9]+(.[0-9])? gwei|wei|eth.",
            "Invalid ethereum amount (mille euro): either unrecognised unit or wrongly formatted number."
        ]);
    });
});

describe('List all published software by self', async () => {
    const ethereumInstance = new EthereumEmulator(new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"));
    const serverInstance = new ServerEmulator(ethereumInstance, ethereumInstance.account);

    let facade = new ReadOnlyCommandsFacade(ethereumInstance, serverInstance);
    let writeFacade = new ReadWriteCommandsFacade(ethereumInstance, serverInstance);

    const swName = "testSoftware";

    it('init: correctly logs in CLI', async () => {

        const address = await ethereumInstance.getAddress();
        const privateKey = await ethereumInstance.account.getPrivateKeyHash();

        const logs: Array<string> = await initFunction(facade, ethereumInstance, address, privateKey);
        //execute and retrieve the console logs and errors of init command

        assert.deepEqual(logs, [
            'Initializing etherless...',
            'succeeded. You can now use all the great power that comes from etherless!'
        ]);

    });

    it('list: should show the software list, which is empty', async () => {

        const logs: Array<string> = await listFunction(facade, new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"), true);
        //execute and retrieve the console logs and errors of list command

        assert.deepEqual(logs, [
            "Listing available software on etherless...",
            logs[1]
        ]);
    });

    it('deploy: correctly deploy a software', async () => {

        const logs: Array<string> = await deployFunction(swName, "./test/examples/sum.js", writeFacade, ethereumInstance);
        //execute and retrieve the console logs and errors of deploy command

        assert.deepEqual(logs, [
            "Requested deploy of testSoftware on etherless, contained in the JS file ./test/examples/sum.js ...",
            `Success! From now on everybody can use ${swName}, you gave power to the world!`,
        ]);
    });

    it('list: should show the software list, showing only one deployed software', async () => {

        const logs: Array<string> = await listFunction(facade, new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df"), true);
        //execute and retrieve the console logs and errors of list command

        assert.deepEqual(logs, [
            "Listing available software on etherless...",
            logs[1]
        ]);
    });
});