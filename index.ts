#!/usr/bin/env node

/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import chalk from "chalk";
import yargs from "yargs";

import {EthereumAccount, EtherlessSmartReadOnly, EtherlessSmartReadWrite, InvalidPrivateKeyException} from "@b-smart/etherless-smart";
import {ReadOnlyCommandsFacade} from "./src/ReadOnlyCommandsFacade";
import {ReadWriteCommandsFacade} from "./src/ReadWriteCommandsFacade";
import {EtherlessCLIConfiguration} from "./src/EtherlessCLIConfiguration";
import {EtherlessServer} from "./src/EtherlessServer";
import {NotInitializedException} from "./src/exceptions/NotInitializedException";

console.log(
    chalk.yellow(
        require("figlet").textSync("etherless", {horizontalLayout: "full"})
    )
);

yargs
    .command("init", "Initialize the application by providing an Ethereum wallet", (yargs) => {
        yargs
            .option("privateKey", {
                alias: "pk",
                type: "string",
                describe: "the private key of the Ethereum account to be used, if not specified a random one will be used",
                default: process.env.ETHERLESS_PRIVATE_KEY === undefined ? EthereumAccount.generatePrivateKey() : process.env.ETHERLESS_PRIVATE_KEY
            })
            .option("serverURI", {
                alias: "server",
                type: "string",
                describe: "the etherless-server hostname/URI (you can provide a default value with ETHERLESS_SERVER_HOSTNAME environment variable)",
                default: typeof process.env.ETHERLESS_SERVER_HOSTNAME === "undefined" ? "https://0xpozzpsa3.execute-api.eu-central-1.amazonaws.com" : process.env.ETHERLESS_SERVER_HOSTNAME
            });
    }, async (argv) => {
        const serverHostname: string = argv.serverURI as string;
        const privateKey: string = argv.privateKey as string;
        const configFilePath: string = argv.config as string;
        
        try{
            const account = new EthereumAccount(privateKey);

            const defaultConfiguration = (new EtherlessCLIConfiguration(serverHostname)).withPrivateKey(account.privateKey);

            const commandFacadeInstance = new ReadOnlyCommandsFacade(
                new EtherlessSmartReadOnly(defaultConfiguration),
                new EtherlessServer(serverHostname)
            );
            const filledConfiguration = await commandFacadeInstance.init(defaultConfiguration, await account.getAddress(), await account.getPrivateKeyHash());

            if (filledConfiguration instanceof EtherlessCLIConfiguration) {
                EtherlessCLIConfiguration.storeToFile(filledConfiguration, configFilePath);
            } else {
                console.error(`${chalk.red("Error")}. Unexpected error in configuration type. Please, contact developer.`);
            }
        } catch(ex) {
            if (ex instanceof InvalidPrivateKeyException) {
                console.error(`${chalk.red("Error")}. The private key is invalid`);
            }
        }
    })
    .command("who", "Shows the wallet in use by etherless",
        async () => {
        // no parameters required
        },
        async (argv) => {
            const configFilePath: string = argv.config as string;

            try {
                const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

                const account = new EthereumAccount(configuration.privateKey);

                const commandFacadeInstance = new ReadOnlyCommandsFacade(
                    new EtherlessSmartReadOnly(configuration),
                    new EtherlessServer(configuration.serverHostname)
                );
                await commandFacadeInstance.who(await account.getAddress());
            } catch (ex) {
                if (ex instanceof NotInitializedException) {
                    console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
                }
            }
        })
    .command("list", "List all software products on etherless", async () => {
        yargs
            .option("self", {
                alias: "me",
                type: "boolean",
                describe: "hide software from everybody else",
                default: false
            });
    },
    async (argv) => {
        const configFilePath: string = argv.config as string;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const account = new EthereumAccount(configuration.privateKey);

            const commandFacadeInstance = new ReadOnlyCommandsFacade(
                new EtherlessSmartReadOnly(configuration),
                new EtherlessServer(configuration.serverHostname)
            );

            const showOnlySelf: boolean = argv.self as boolean;

            await commandFacadeInstance.list(await account.getAddress(), showOnlySelf);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("find softwareName", "Find a software on etherless", (yargs) => {
        yargs
            .positional("softwareName", {
                describe: "name of the software to search on etherless",
                default: ""
            });
    }, async (argv) => {
        const swName: string = argv.softwareName as string;
        const configFilePath: string = argv.config as string;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const commandFacadeInstance = new ReadOnlyCommandsFacade(
                new EtherlessSmartReadOnly(configuration),
                new EtherlessServer(configuration.serverHostname)
            );
            await commandFacadeInstance.find(swName);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("deploy softwareName filepath", "Deploy a software product ", (yargs) => {
        yargs
            .positional("softwareName", {
                describe: "name of the software to deploy on etherless"
            })
            .positional("filepath", {
                describe: "path to the software to upload on etherless"
            })
            .option("gasLimit", {
                alias: "gl",
                type: "number",
                describe: "The gas limit",
                default: 2700000
            })
            .option("gasPrice", {
                alias: "gp",
                type: "string",
                describe: "The price of a gas unit",
                default: "9.0"
            })
            .option("gasUnit", {
                alias: "gu",
                type: "string",
                describe: "The measure unit of a gas unit",
                default: "gwei"
            });
    }, async (argv) => {
        const name: string = argv.softwareName as string;
        const filepath: string = argv.filepath as string;
        const configFilePath: string = argv.config as string;

        const gasLimit: number = argv.gasLimit as number;
        const gasPrice = `${argv.gasPrice}`;
        const gasUnit = `${argv.gasUnit}`;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const commandFacadeInstance = new ReadWriteCommandsFacade(
                new EtherlessSmartReadWrite(configuration, new EthereumAccount(configuration.privateKey)),
                new EtherlessServer(configuration.serverHostname)
            );
            await commandFacadeInstance.deploy(name, filepath, gasLimit, gasPrice, gasUnit);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("edit softwareName filepath", "Update a software product ", (yargs) => {
        yargs
            .positional("softwareName", {
                describe: "name of the software to update on etherless"
            })
            .positional("filepath", {
                describe: "path to the software that will replace the previous version on etherless"
            })
            .option("gasLimit", {
                alias: "gl",
                type: "number",
                describe: "The gas limit",
                default: 2700000
            })
            .option("gasPrice", {
                alias: "gp",
                type: "string",
                describe: "The price of a gas unit",
                default: "9.0"
            })
            .option("gasUnit", {
                alias: "gu",
                type: "string",
                describe: "The measure unit of a gas unit",
                default: "gwei"
            });
    }, async (argv) => {
        const name: string = argv.softwareName as string;
        const filepath: string = argv.filepath as string;
        const configFilePath: string = argv.config as string;

        const gasLimit: number = argv.gasLimit as number;
        const gasPrice = `${argv.gasPrice}`;
        const gasUnit = `${argv.gasUnit}`;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const commandFacadeInstance = new ReadWriteCommandsFacade(
                new EtherlessSmartReadWrite(configuration, new EthereumAccount(configuration.privateKey)),
                new EtherlessServer(configuration.serverHostname)
            );
            await commandFacadeInstance.update(name, filepath, gasLimit, gasPrice, gasUnit);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("run softwareName [args]", "Execute a software on etherless", (yargs) => {
        yargs
            .positional("softwareName", {
                describe: "name of the software to run on etherless"
            })
            .positional("args", {
                describe: "the list of parameters, separated by a comma",
                default: ""
            })
            .option("gasLimit", {
                alias: "gl",
                type: "number",
                describe: "The gas limit",
                default: 2700000
            })
            .option("gasPrice", {
                alias: "gp",
                type: "string",
                describe: "The price of a gas unit",
                default: "9.0"
            })
            .option("gasUnit", {
                alias: "gu",
                type: "string",
                describe: "The measure unit of a gas unit",
                default: "gwei"
            });
    }, async (argv) => {
        const name: string = argv.softwareName as string;
        const argsString: string = argv.args as string;
        const executionArgs: string[] = (argsString.length > 0) ? argsString.split(",") : [];
        const configFilePath: string = argv.config as string;

        const gasLimit: number = argv.gasLimit as number;
        const gasPrice = `${argv.gasPrice}`;
        const gasUnit = `${argv.gasUnit}`;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const commandFacadeInstance = new ReadWriteCommandsFacade(
                new EtherlessSmartReadWrite(configuration, new EthereumAccount(configuration.privateKey)),
                new EtherlessServer(configuration.serverHostname)
            );
            await commandFacadeInstance.run(name, executionArgs, gasLimit, gasPrice, gasUnit);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("history", "Shows the list of previous executions", () => {
        // No additional params
    }, async (argv) => {
        const configFilePath: string = argv.config as string;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const account = new EthereumAccount(configuration.privateKey);

            const commandFacadeInstance = new ReadOnlyCommandsFacade(
                new EtherlessSmartReadOnly(configuration),
                new EtherlessServer(configuration.serverHostname)
            );

            await commandFacadeInstance.history(await account.getAddress());
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("log [execID]", "Display informations about a specific execution", (yargs) => {
        yargs
            .positional("execID", {
                describe: "the unique ID of a software execution",
                type: "string",
                default: ""
            });
    }, async (argv) => {
        const execID: string = argv.execID as string;
        const configFilePath: string = argv.config as string;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const account = new EthereumAccount(configuration.privateKey);

            const commandFacadeInstance = new ReadOnlyCommandsFacade(
                new EtherlessSmartReadOnly(configuration),
                new EtherlessServer(configuration.serverHostname)
            );
            await commandFacadeInstance.log(await account.getAddress(), await account.getPrivateKeyHash(), execID);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("delete softwareName", "Delete a software from etherless", (yargs) => {
        yargs
            .positional("softwareName", {
                describe: "name of the software to delete from etherless"
            })
            .option("gasLimit", {
                alias: "gl",
                type: "number",
                describe: "The gas limit",
                default: 3000000
            })
            .option("gasPrice", {
                alias: "gp",
                type: "string",
                describe: "The price of a gas unit",
                default: "9.0"
            })
            .option("gasUnit", {
                alias: "gu",
                type: "string",
                describe: "The measure unit of a gas unit",
                default: "gwei"
            });
    }, async (argv) => {
        const name: string = argv.softwareName as string;

        const configFilePath: string = argv.config as string;

        const gasLimit: number = argv.gasLimit as number;
        const gasPrice = `${argv.gasPrice}`;
        const gasUnit = `${argv.gasUnit}`;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const commandFacadeInstance = new ReadWriteCommandsFacade(
                new EtherlessSmartReadWrite(configuration, new EthereumAccount(configuration.privateKey)),
                new EtherlessServer(configuration.serverHostname)
            );
            await commandFacadeInstance.remove(name, gasLimit, gasPrice, gasUnit);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("deposit amount unit", "Deposit an amount of ethers on etherless to pay for software executions", (yargs) => {
        yargs
            .positional("amount", {
                describe: "amount of value to be sent to etherless",
                default: "500.0"
            })
            .positional("unit", {
                describe: "unit for the amount of value to be sent to etherless (ether, gwei, wei)",
                default: "gwei"
            })
            .option("gasLimit", {
                alias: "gl",
                type: "number",
                describe: "The gas limit",
                default: 3000000
            })
            .option("gasPrice", {
                alias: "gp",
                type: "string",
                describe: "The price of a gas unit",
                default: "9.0"
            })
            .option("gasUnit", {
                alias: "gu",
                type: "string",
                describe: "The measure unit of a gas unit",
                default: "gwei"
            });
    }, async (argv) => {
        const configFilePath: string = argv.config as string;

        const amount: string = (argv.amount as number).toString();
        const unit: string = argv.unit as string;

        const gasLimit: number = argv.gasLimit as number;
        const gasPrice = `${argv.gasPrice}`;
        const gasUnit = `${argv.gasUnit}`;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const commandFacadeInstance = new ReadWriteCommandsFacade(
                new EtherlessSmartReadWrite(configuration, new EthereumAccount(configuration.privateKey)),
                new EtherlessServer(configuration.serverHostname)
            );
            await commandFacadeInstance.deposit(amount, unit.toLowerCase(), gasLimit, gasPrice, gasUnit);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .command("withdraw", "Withdraw the deposited amount on etherless", () => {
        yargs
            .option("gasLimit", {
                alias: "gl",
                type: "number",
                describe: "The gas limit",
                default: 3000000
            })
            .option("gasPrice", {
                alias: "gp",
                type: "string",
                describe: "The price of a gas unit",
                default: "9.0"
            })
            .option("gasUnit", {
                alias: "gu",
                type: "string",
                describe: "The measure unit of a gas unit",
                default: "gwei"
            });
    }, async (argv) => {
        const configFilePath: string = argv.config as string;

        const gasLimit: number = argv.gasLimit as number;
        const gasPrice = `${argv.gasPrice}`;
        const gasUnit = `${argv.gasUnit}`;

        try {
            const configuration = await EtherlessCLIConfiguration.loadFromFile(configFilePath);

            const commandFacadeInstance = new ReadWriteCommandsFacade(
                new EtherlessSmartReadWrite(configuration, new EthereumAccount(configuration.privateKey)),
                new EtherlessServer(configuration.serverHostname)
            );
            await commandFacadeInstance.withdraw(gasLimit, gasPrice, gasUnit);
        } catch (ex) {
            if (ex instanceof NotInitializedException) {
                console.error(`${chalk.red("Error")}. etherless has not been initialized. Run etherless init to get ${chalk.italic("power")}!`);
            }
        }
    })
    .option("config", {
        alias: "c",
        type: "string",
        description: "The file that stores etherless configuration and the Ethereum account to be used",
        default: `${process.env.HOME}/etherless.json`
    })
    .usage("Usage: etherless [command] [options]")
    .argv;
