# etherless-cli

## References

https://www.math.unipd.it/~tullio/IS-1/2019/Progetto/C2.pdf

Software Engineering project at University of Padua, developed for [Red Babel](http://redbabel.com/), by B.smart.

## Installation

- Clone the project;
- Run the container (sugged through VSCode remote-containers);

## Build

etherless-cli is written in TypeScript and the js code needs to be generated:
The following command run the build of .js files, providing static code analysis with ESLint

```sh
npm install && npm run build
chmod +x lib/index.js && npm install -g
```

## Run 

Thanks to npm install -g, we can launch our application by: 
```sh
etherless
```

## Static analysis

ESLint is configured to provide static code analysis

The following command provides the static check declared in .eslintrc

```sh
npm run lint
```

The following command provides the static check declared in .eslintrc, automatically correct basic mistakes

```sh
npm run lint-and-fix
```

## Testing

Local tests (mocha and chai) can be run using
```sh
npm run test
```

## Ropsten Account

Use this account on cli:

Alternative (ropsten): 0xfeaeaff0284006500a37882871f69599ef212d05ab9fcd091b50529388a2f49e

```
PrivateKey: 0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df
Address: 0xb6E3d47f16FD4d25a4b8d193B2bC4C5C8F4E555D
```
